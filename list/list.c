/*
 * list.c
 * 
 * Copyright 2020 Unknown <sferd@agesa>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */

#define _LIST_C
#include "list.h"

/* Инициализация */
list * mkList()
{
	list * list1 = malloc( sizeof( list));
	
	list->count = 0;

	list->first = NULL;
	list->last = NULL;
	
	return list1;
}

list_node * mkListNode( void * data)
{
	list_node * node1 = malloc( sizeof( list_node));
	
	node1->data = data;
	
	node1->next = NULL;
	node1->last = NULL;
	
	return node1;
}

/* Связывание узлов */
inline void lnlink( list_node * first, list_node * second)
{
	first->next = second;
	second->prev = first;
	
	return;
}

/* Перемещение по списку и получение данных по порядковому номеру */
inline void * lget( list list1, int position)
{
	return lgoto( list1, position)->data;
}

inline static int correctedIndex( list * list1, int position) // Обработка индекса
{
	if ( position <= list1->count || position >= 0)
	{
		return position;
	}
	
	if ( position < 0 || position >= -list1->count)
	{	
		position = list1->count + position;		// Обработка отрицательного индекса, как доступ с конца
	
		return position;
	}
	
	perror( "FATAL ERROR: You trying to get an item that is out of the list");
	abort();
}

list_node * lgoto( list * list1, int position)	// Возвращает адрес узла по порядковому номеру
{	
	position = correctedIndex( list1, position);	// Приводим отрицательные индексы к аналогичным с конца
	
	list_node * node1;
	
	if ( position * 2 <= list1->count)				// Поиск минимального маршрута с начала или с конца
	{
		node1 = list1->first;
		
		for( int i = 0; i < position; ++i)
			node1 = node1->next;
	}
	else
	{
		node1 = list1->last;
		
		for( int i = 0; i < position; ++i)
			node1 = node1->prev;
	}
	
	return node1;
}

/* Добавление элементов */

static void laddnode( list * list1, list_node * added_node, list_node * target_node)
{
	// Добавление узла в лист на место указанного
	// Остальные смещаются
	
	if ( target_node == NULL)
	{
		if ( list1->first == NULL)				// Для пустого листа
		{	
			list1->first = added_node;
			list1->last = added_node;
		
			list1->count = 1;	
		}
		else									// Добавление в конец
		{
			lnlink( list1->last, added_node);
			list1->last = added_node;
		}
	}
	else
	{
		lnlink( target_node->prev, added_node);
		lnlink( added_node, target_node);
	}
	
	++list1->count;
	
	return;
}

void lappend( list * list1, void * data)	// Добавление элемента в конец
{
	list_node * added_node = mkListNode( data);
	
	laddnode( list1, added_node, NULL);		// Добавляем узел в конец
	
	return;
}

int lextend( list * list1, list * added_list)	// Добавление листа в конец другого листа (вставляемый лист не сохраняется)
{
	int added_count = added_list->count;		// Число добавляемых элементов
	
	lnlink( list1->last, added_list->first);	// Создаём связь между листами
	list1->last = added_list->last;				// Перемещаем указатель конца на конец другого списка
	
	free( added_list);
	
	list1->count += added_count;
	
	return added_count;							// Доавляется число добавленных элементов
}

void linsert( list * list1, list_node * added_node, int position)	// Добавление на позицию
{
	laddnode( list1, added_node, lgoto( list1, position));
}

/* Поиск */
static int lnturn( list_node ** node_ptr, int max_step, void * value, void (* value_cmp)( void *, void *))	// Базовый поиск
{	
	list_node * cur_node = * node_ptr;	// Изменяемый указатель
	int i = 0;							// Счётчик шагов
	
	while( i <= abs( max_step) && cur_node != NULL)	// Поиск первого совпадения
	{
		if ( ! value_cmp( value, cur_node->data))	// Обработка совпадения
		{
			* node_ptr = cur_node;
			return i * SIGNOF( max_step);			// Возвращаем число шагов со знаком направления
		}
		
		++i;
		if ( max > 0)								// Знак max_step определяет направление поиска
			cur_node = cur_node->next;
		else
			cur_node = cur_node->prec;
	}

	* node_ptr = NULL;	// Индикатор того что в фрагменте списка не был найден элемент
	return i;			// Расстояние до конца листа или max_steps
}

list_node * lnfind( list * list1, void * value, void (* value_cmp)( void *, void *))	// Поиск по всему листу
{
	list_node * cur_node = list1->first;
	
	lnturn( &cur_node, MAX_INT, value, value_cmp);	// Поиск первого вхождения для элемента после указанного узла
	
	return cur_node;
}

list_node * lnfindr( list1, int start_pos, int end_pos, void * value, void (* value_cmp)( void *, void *))	// Поиск в промежутке
{
	int step = start_pos - end_pos;
	//	Число проходимых шагов.
	//	Будет проверенно abs( step) + 1 Элементов.
	
	list_node * cur_node = lgoto( list1, start_pos); 	// Перемещение на первую позицию
	
	lnturn( &cur_node, step, value, value_cmp);			// Поиск
	
	return cur_node;
}

/* Изъятие из листа */

inline static void list_node lnsieze( list * list1, list_node removed_node)
{
	lnlink( removed_node->prev, removed_node->next);
	
	removed_node->prev = NULL;
	removed_node->next = NULL;
	
	--list1->count;
	
	return;
}

void * lpop( list list1, int position)	// Выбрасываем элемент из списка по позиции 
{
	list_node * removed_node = lgoto( list1, position);
	lnsieze( list1, removed_node);
	
	void * data = removed_node->data; 
	free( removed_node);
	
	return * data;
}

void lpopfree( list list1, int position, void ( * freedom)( void * ptr))	// Выбрасываем элемент по значению
{
	void * data = lpop( list1, position);
	freedom( data);
	
	return;
}

inline void lremove( list list1, void * data, void (* value_cmp)( void *, void *), void ( * freedom)( void * ptr))
{
	list_node * removed_node = lnfind( list1, data, value_cmp);
	lsieze( list1, removed_node);
	
	freedom( removed_node->data);
	free( removed_node);
	
	return;
}

/* Очистка памяти */
void lfree( list * list1, void ( * freedom)( void * ptr))
{
	// Освобождает память, выделяемую под список и все включаемые данные

	lclear( list1, freedom);
	free( list1);
	
	return;
}

void lclear( list * list1, void ( * freedom)( void * ptr))
{	
	// Освобождает память всех включенных в список узлов и память включаемых данных
	
	list_node * cur_node = list1->first;
	list_node * next_ptr;

	while ( cur_node != NULL)
	{
		next_ptr = cur_node->next;
		
		freedom( cur_node->data);
		free( cur_node);
		
		cur_node = cur_node->next;
	}
	
	return;
}
