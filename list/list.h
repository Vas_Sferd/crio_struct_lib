/*
 * list.h
 * 
 * Copyright 2020 Unknown <sferd@agesa>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */

#ifndef _LIST_H
#define _LIST_H

typedef struct
{
	int count;		// Число элементов
	
	void * first;	// Первый элемент
	void * last;	// Последний
	
} list;

typedef struct
{
	void * data; // Данные
	
	void * prev; // Указатель на предыдущий элемент
	void * next; // Указатель на следующий элемент
	
} list_node;

/**********************************************************************/
#ifdef _LIST_C

	#include <limits.h>
	#include <math.h>
	#include <stdlib.h>

	#define SIGNOF( X) ( ( (X) > 0) - ( (X) < 0)  )

#endif

/**********************************************************************/

/* Инициализация */
list * mkList();
list_node * mkListNode( void * data);

/* Связывание узлов */
inline void lnlink( list_node * first, list_node * second);

/* Перемещение по списку и получение данных по порядковому номеру */
inline void * lget( list list1, int position);
list_node * lgoto( list * list1, int position);								// Возвращает адрес узла по порядковому номеру

/* Добавление элементов */
void lappend( list * list1, void * data);									// Добавление элемента в конец
int lextend( list * list1, list * added_list);								// Добавление листа в конец другого листа (вставляемый лист не сохраняется)
void linsert( list * list1, list_node * added_node, int position);			// Добавление на позицию

/* Изъятие из листа */
void * lpop( list list1, int position);										// Выбрасываем элемент из списка по позиции 
void lpopfree( list list1, int position, void ( * freedom)( void * ptr));	// Выбрасываем элемент по значению

/* Очистка памяти */
void lfree( list * list1, void ( * freedom)( void * ptr));					// Очистка и удаление списка
void lclear( list * list1, void ( * freedom)( void * ptr));					// Очистка списка

#endif
